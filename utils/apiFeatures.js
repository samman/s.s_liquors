module.exports = class ApiFeatures{

    constructor(dbQuery, query){
        this.dbQuery = dbQuery;
        this.query = query;
        this.pages = null;
    }

    filter(){
        let queryCopy = {...this.query};
        const excludedFields = ["sort", "fields", "paginate", "limit"];
        excludedFields.forEach(field=>{
            delete queryCopy[field];
        });
        queryCopy = JSON.stringify(queryCopy);
        queryCopy.replace(/b[gt|lt|gte|lte]b/g, match=>`$${match}`);
        queryCopy = JSON.parse(queryCopy);
        
        this.dbQuery = this.dbQuery.find(queryCopy);
        return this;
    }

    sort(){
        if(this.query.sort){
            let sort = this.query.sort;
            sort = sort.replace(/,/g, " ");
            this.dbQuery = this.dbQuery.sort(sort);
        }
        return this;
    }

    limitFields(){
        let fields;
        if(this.query.fields){
            fields = this.query.fileds;
            fields = fields.replace(/,/g, " ");
        }else{
            fields = "-__v";
        }
        this.dbQuery = this.dbQuery.select(fields);
        return this;
    }

    paginate(){
        const page = this.query.page*1 || 1;
        const limit = this.query.limit*1 || 10;

        const skip = (page-1)*limit;   
        
        // this.pages = copyQuery.countDocuments({}, function(err, count){
        //     return count.length
        // })

        this.dbQuery = this.dbQuery.skip(skip).limit(limit);
        return this;
    }

    // countPages(){
    //     const copyQuery = Object.assign(this.dbQuery)
    //     this.pages = copyQuery.countDocuments({}, function(err, count){
    //         return count.length
    //     })
    //     console.log("I am here")
    //     return this;
    // }

}