const multer = require('multer');

const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const imageUploader = require('../utils/imageUploader');
const ApiFeatures = require("../utils/apiFeatures");

const User = require('../models/userModel');
const UserDrink = require("../models/userDrinkModel");

exports.storeProfilePic = imageUploader("profilePic","images/profile", {
    fileFor: 'user'
});

exports.setProfilePic = catchAsync(async (req,res)=>{
    const user = await User.findByIdAndUpdate(req.user._id, { profilePic:  req.file.path }, {
        new: true,
        runValidation: true
    });
    res.status(200).json({
        status: "success",
        data: {
            user
        }
    })
})

exports.updateMe = catchAsync(async(req,res)=>{
    res.status(200).send("Successfully updated")
})

exports.getUserDetails = catchAsync(async(req,res)=>{
    const user = {...req.user._doc};
    delete user.role;

    res.status(200).json({
        status: "success",
        data: {
            user
        }
    })
})

exports.getUserOrders = catchAsync(async(req,res)=>{
    const apiFeature = new ApiFeatures(UserDrink.find({user_id: req.user._id, status: {$ne:"cancelled"}}), req.query).filter().sort().limitFields().paginate();

    const orders = await apiFeature.dbQuery;

    res.status(200).json({
        status: 'success',
        data: {
            orders
        } 
    })
})