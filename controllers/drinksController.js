const catchAsync = require("../utils/catchAsync");
const AppFeature = require('../utils/apiFeatures');
const imageUploader = require("../utils/imageUploader");

const Drink = require("../models/drinkModel");

exports.storeImage = imageUploader("image","images/drinks", {
    fileFor: 'drink'
});

exports.getDrinks = catchAsync(async(req,res)=>{
    const appFeature = new AppFeature(Drink.find(), req.query).filter().sort().limitFields().paginate();
    const drinks = await appFeature.dbQuery;
    return res.status(200).json({
        status: "success",
        data: {
            drinks
        }
    })
})

exports.getImage = catchAsync(async(req,res)=>{
    const drink = await Drink.findById(req.params.id);
    res.status(200).sendFile(drink.image, { root: `${__dirname}/../` });
})

exports.uploadImage = catchAsync(async(req,res,next)=>{
    const drink = await Drink.findByIdAndUpdate(req.params.id, {
        image: req.file.path
    }, {
        new: true,
        runValidation: true
    });
    res.status(201).json({
        status: "success",
        data: {
            drink
        }
    })
})

exports.createDrink = catchAsync(async(req,res,next)=>{
    const drink = await Drink.create(req.body);
    res.status(201).json({
        status: "success",
        data:{ 
            drink
        }
    });
})

exports.searchDrink = catchAsync(async(req,res,next)=>{
    const appFeature = new AppFeature(Drink.find({name: { $regex: req.query.term }}), req.query)
        .sort()
        .limitFields()
        .paginate();
    const drinks = await appFeature.dbQuery;
    res.status(200).json({
        status: 'success',
        data: {
            drinks
        }
    })
})
