const catchAsync = require("../utils/catchAsync");
const ApiFeatures = require("../utils/apiFeatures");
const UserDrink = require('../models/userDrinkModel');

exports.orderDrink = catchAsync(async (req,res)=>{
    const userDrink = await UserDrink.create({
        user_id: req.user._id,
        drinks: req.body,
        status: "pending"
    })
    res.status(201).json({
        status: "success",
        data: {
            user: userDrink.populate("user"),
            drink: userDrink.populate("drink")
        }
    })
})

exports.updateOrder = catchAsync(async(req,res)=>{
    const userDrink = await UserDrink.findByIdAndUpdate(req.params.id, req.body, {
        runValidators: true,
        new: true
    });
    res.status(200).json({
        status: "success",
        data: {
            order:userDrink
        }
    })
})

exports.getOrders = catchAsync(async(req,res)=>{

    const apiFeature = new ApiFeatures(UserDrink.find(), req.query).filter().sort().limitFields().paginate();

    const orders = await apiFeature.dbQuery;

    res.status(200).json({
        status: 'success',
        data: {
            orders
        } 
    })
})