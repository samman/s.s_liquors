import axios from "axios";

const BASE_URL = "api/v1/auth"

export default {

    login(form){
        return axios.post(`${BASE_URL}/login`, form)
    },

    signup(form){
        return axios.post(`${BASE_URL}/signup`, form)
    },

}