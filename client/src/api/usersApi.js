import axios from "axios";

const BASE_URL = "api/v1/users";

export default {
    uploadProfilePic(profilePic, token){
        const formData = new FormData();
        formData.append("profilePic", profilePic);
        return axios.patch(`${BASE_URL}/updateProfilePic`,formData,{
            headers: {
                "Content-Type": "multipart/form-data",
                Authorization: `Bearer ${token}`
            }
        });
    }
}