import axios from "axios";

const BASE_URL = "api/v1/drinks"

export default {

    create(form,token){
        return axios.post(BASE_URL, form, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
    },

    uploadImage(drinkId, img, token){
        const formData = new FormData();
        formData.append("image", img);
        return axios.post(`${BASE_URL}/${drinkId}/upload-image`, formData, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
    },

    getDrinks(searchTerm, token){
        return axios.get(`${BASE_URL}/search`, {
            params: {
                term: searchTerm,
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
    },

    getImage(id, token){
        return axios.get(`${BASE_URL}/${id}/get-image`, {
            headers:{
                'Authorization': `Bearer ${token}`
            }
        })
    }

}