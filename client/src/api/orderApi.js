import axios from "axios";

const BASE_URL = "api/v1";

export default {
    
    orderDrinks(drinks, token){
        return axios.post(`${BASE_URL}/orders`, drinks, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
    },

    fetchOrders(token){
        return axios.get(`${BASE_URL}/orders?sort=-order_date`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
    },

    updateOrder(data, token){
        return axios.patch(`${BASE_URL}/orders/${data.id}`, data.body, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });
        
    }
}