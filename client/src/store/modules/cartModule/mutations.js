export default {
    addDrinkToCart(state, drink){
        for(let cartDrink of state.cart){
            if(cartDrink._id==drink._id && cartDrink.selectedVol==drink.selectedVol){
                return cartDrink.selectedQuantity++;
            }
        }
        state.cart.push(drink);
    },
    removeAllCartDrinks(state){
        state.cart = [];
    },
    removeOneDrinkFromCart(state, drink_id){
        state.cart = state.cart.filter(drink => {
            return drink._id !== drink_id;
        })
    }
}