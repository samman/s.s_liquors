export default{
    addCartDrink(context, drink){
        context.commit("addDrinkToCart", drink);
    }, 

    emptyCart(context){
        context.commit("removeAllCartDrinks");
    },
    
    removeOne(context, drink_id){
        context.commit("removeOneDrinkFromCart", drink_id)
    }
}