import authModule from './authModule';
import drinksModule from './drinksModule';
import cartModule from './cartModule';
import ordersModule from "./ordersModule";

export default {
    authModule,
    drinksModule,
    cartModule,
    ordersModule
}