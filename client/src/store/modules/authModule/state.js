export default {
    token: window.localStorage.getItem("apiToken"),
    role: window.localStorage.getItem("role")
}