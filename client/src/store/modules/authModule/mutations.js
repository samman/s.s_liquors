export default {
    setToken(state, token){
        state.token = token;
    },
    setRole(state, role){
        state.role = role;
    },
    removeToken(state){
        state.token = null;
    },
    removeRole(state){
        state.token = null;
    }
}