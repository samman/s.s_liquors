import authApi from "../../../api/authApi";
import usersApi from "../../../api/usersApi";

export default{

    async login(context, form){
        const res = await authApi.login(form);
        const token = res.data.token;
        const role = res.data.role;
        storeTokenAndRole(token, role);
        context.commit("setToken", token);
        context.commit("setRole", role);
    },
    async signup(context, form){
        const res = await authApi.signup(form);
        const token = res.data.token
        const role = res.data.role;
        storeTokenAndRole(token, role);
        context.commit("setToken", token);
        context.commit("setRole", role);

        
        if(form.profilePic){
            await usersApi.uploadProfilePic(form.profilePic, context.getters.getToken);
        }
    },

    logout(context){
        removeTokenAndRole();
        context.commit("removeToken");
        context.commit("removeRole");
        window.location.assign("/login");
        console.log("ok")
    }
}

function storeTokenAndRole(token, role){
    window.localStorage.setItem("apiToken", token);
    window.localStorage.setItem("role", role)
}

function removeTokenAndRole(){
    window.localStorage.removeItem("apiToken");
    window.localStorage.removeItem("role")
}