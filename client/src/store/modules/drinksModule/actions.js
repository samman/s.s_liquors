import drinksApi from "../../../api/drinksApi";

export default{
    async createDrink(context, form){
        const formCopy = {...form};
        delete formCopy.image;
        const res = await drinksApi.create(formCopy, getToken(context));
        if(res.status==201 && form.image){
            await drinksApi.uploadImage(res.data.data.drink._id, form.image, window.localStorage.getItem("apiToken"));
        }
    },

    async fetchDrinks(context, searchTerm){
        const res = await drinksApi.getDrinks(searchTerm, getToken(context));
        const drinks = res.data.data.drinks;
        const updatedDrinks = drinks.map(item=>{
            if(item.image){
                // item.image = item.image.replace("public", "");
            }
            return item;
        })
        context.commit("setDrinks", updatedDrinks)
    },
    
}

function getToken(context){
    return context.rootState.authModule.token;
}