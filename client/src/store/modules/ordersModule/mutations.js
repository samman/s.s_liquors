export default {

    setOrders(state, orders){
        state.orders = orders
    },

    updateOrder(state, payload){
        const order =  state.orders.find(order=>order._id==payload.id)
        for(let key of Object.keys(payload.data)){
            if(order[key])  order[key] = payload.data[key]
        }
    },

    removeOrder(state,id){
        state.orders = state.orders.filter(order=>order._id!=id)
    }

}