import orderApi from "../../../api/orderApi"

export default {

    async orderDrinks(context, drinks){
        await orderApi.orderDrinks(drinks, getToken(context))
    },

    async fetchOrders(context){
        const res = await orderApi.fetchOrders(getToken(context));
        context.commit("setOrders",res.data.data.orders);
    },

    async orderStatusUpdate(context, payload){
        const res = await orderApi.updateOrder(payload, getToken(context));
        if(res.data.data.order.status=='cancelled'){
            context.commit("removeOrder", payload.id);
            return;
        }
        context.commit("updateOrder", {id: payload.id, data: {status: res.data.data.order.status}})
    }

}

function getToken(context){
    return context.rootState.authModule.token;
}