import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";

import DrinkForm from "./components/DrinkForm";
import Drinks from "./components/Drinks";
import Cart from "./components/Cart";
import OrderHistory from "./components/OrderHistory";

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: ()=>import("./pages/App.vue"),
            beforeEnter: (to, from, next)=>{
                if(store.state.authModule.token){
                    next();
                }
                else{
                    next("/login");
                }
            },
            children: [
                {
                    path: "/",
                    component: Drinks
                },
                {
                    path: "/new-drink",
                    component: DrinkForm
                },
                {
                    path: "/cart",
                    component: Cart
                },
                {
                    path: "/history",
                    component: OrderHistory
                }
            ]
        },
        {
            path: "/login",
            component: ()=>import("./pages/Login.vue"),
            beforeEnter: (to, from, next)=>{
                if(!store.state.authModule.token){
                    next();
                }else{
                    next("/");
                }
            }
        },
        {
            path: "/signup",
            component: ()=>import("./pages/Signup.vue")
        },
        {
            path: "*",
            component: ()=>import("./pages/404NotFound.vue")
        }
    ]
})
