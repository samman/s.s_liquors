const router = require("express").Router();

const drinksController = require("../controllers/drinksController");
const authController = require("../controllers/authController");

router
    .route("/")
    .get(authController.protect, drinksController.getDrinks)
    .post(
        authController.protect,
        authController.restrictTo("store-owner"),
        drinksController.createDrink
    )

router
        .route("/:id/upload-image")
        .post(
            authController.protect,
            drinksController.storeImage,
            drinksController.uploadImage
        )

router
        .route("/:id/get-image")
        .get(
            authController.protect,
            drinksController.getImage
        )

router
        .route("/search")
        .get(
            authController.protect,
            drinksController.searchDrink
        )

module.exports = router;