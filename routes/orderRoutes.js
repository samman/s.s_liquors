const router = require('express').Router();

const authController = require("../controllers/authController");
const orderController = require("../controllers/orderController");
const userController = require("../controllers/userController");

router
    .route("")
    .post(authController.protect, authController.restrictTo("customer"),orderController.orderDrink)
    .get(authController.protect, (req,res,next)=>{
        if(req.user.role=="store-owner"){
            return orderController.getOrders(req,res,next);
        }else{
            return userController.getUserOrders(req,res,next);
        }
    });

router
    .route("/:id")
    .patch(authController.protect, orderController.updateOrder)

module.exports = router;