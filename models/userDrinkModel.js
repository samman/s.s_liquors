const mongoose = require('mongoose');

const userDrinkSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Types.ObjectId,
        ref:"User",
        required: [true, "User ID is required"]
    },
    drinks: {
        type: [new mongoose.Schema({
            drink_id: {
                type: mongoose.Types.ObjectId,
                ref: "Drink",
                required: true
            },
            name:{
                type: String,
                require: true
            },
            volume: {
                type: Number,
                required: true
            },
            quantity: {
                type: Number,
                required: true
            },
        })],
        minlength: 1
    },
    status: {
        type: String,
        enum: ["pending", "received", "cancelled", "rejected"]
    },
    order_date: {
        type: Date,
        default: Date.now()
    },
    received_confirmed: {
        type: Date,
    }
})

module.exports = mongoose.model("UserDrink", userDrinkSchema);