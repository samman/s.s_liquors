const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const validator = require('validator');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A user must have a name"]
    },
    username: {
        type: String,
        required: [true, "A user must have a username"],
        unique: [true, "This username has already been taken"]
    },
    email: {
        type: String,
        required: [true, "A user must have an email"],
        unique: [true, "An account with this email already exists"],
        validate: [validator.isEmail, "Invalid email"]
    },
    profilePic: String,
    password: {
        type: String,
        required: [true, "A user must have a password"],
        select: false
    },
    passwordConfirm: {
        type: String,
        require: [true, "A user must have confirm password"],
        validate: {
            validator: function(val){
                return val === this.password
            },
            message: "Password and Confirm Password doesn't match"
        },
        select: false
    },
    role: {
        type: String,
        default: "customer",
        enum: ["customer", "store-owner"]
    }
})

userSchema.pre("save", async function(next){
    if(!this.isModified('password'))    return next();
    this.password = await bcrypt.hash(this.password, 12);
    this.passwordConfirm = null;
    next();
})

userSchema.methods.correctPassword = async function(candidatePassword, hashedPassword){
    return await bcrypt.compare(candidatePassword, hashedPassword);
}

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;