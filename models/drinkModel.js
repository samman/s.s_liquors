const mongoose = require("mongoose");

const drinkSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A drink must have a name"],
        unique: [true, "Drink with this name already exists"]
    },
    brand: {
        type: String,
        required: [true, "A drink must have a brand"]
    },
    volumes: [{
        volume: {
            type: Number,
            required: [true, "Volume for a drink must be specified"]
        },
        stock_count: {
            type: Number,
            required: [true, "The number of drinks in stock must be specified"]
        },
        price: {
            type: Number,
            require: [true, "The price for a drink must be specified"]
        }
    }],
    alcohol_content: {
        type: Number,
        max: [100, "Alcohol content cannot exceede 100%"],
        min: [0, "Alcohol content cannot be lower than 0%"]
    },
    image: String,
})

module.exports = mongoose.model("Drink", drinkSchema)