const express = require('express');
const morgan = require('morgan');
const cors = require('cors')

//Global Error Hanlder
const errorHandler = require('./controllers/errorHandler');

const authRouter = require('./routes/authRoutes');
const userRouter = require('./routes/userRoutes');
const drinkRouter = require('./routes/drinkRoutes');
const orderRouter = require('./routes/orderRoutes');

const app = express();

if(process.env.NODE_ENV=="development"){
    app.use(morgan("dev"));
}

app.use(express.json());
app.use(cors());

app.use((req,res,next)=>{
    next();
})

const BASE_ROUTE = "/api/v1"

app.use(`${BASE_ROUTE}/auth`,authRouter);
app.use(`${BASE_ROUTE}/users`,userRouter);
app.use(`${BASE_ROUTE}/drinks`, drinkRouter);
app.use(`${BASE_ROUTE}/orders`, orderRouter);

app.use(`/storage`, express.static(`${__dirname}/storage`));


if(process.env.NODE_ENV=="production"){
    app.use("/", express.static(`${__dirname}/public`));
    app.get("*", (req,res)=>{
        res.sendFile(`${__dirname}/public/build/index.html`);
    })
}

app.use("*", (req,res)=>{
    res.status(404).json({
        status: "fail",
        message: "Not found"
    })
})

app.use(errorHandler);

module.exports = app;