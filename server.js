const dotenv = require('dotenv');
const mongoose = require('mongoose');

process.on('uncaughtException', err => {
    console.log("UNCAUGHT EXCEPTION: ");
    console.log(`${err}`);
    process.exit(1);
})

//configuring the environment variables
dotenv.config({ path: `${__dirname}/config.env` });

//Connecting to database
const db = process.env.DATABASE_PROD.replace("<password>", process.env.DATABASE_PASSWORD).replace("<dbname>", process.env.DATABASE_USERNAME);

mongoose.connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    },  
    (err)=>{
    if(err){
        if(process.env.NODE_ENV == 'development'){
            console.log(err);
        }else{
            console.log("SORRY! DATABASE CONNECTION FAILED");
        }
        process.exit(1);
    }
    console.log("Database connected successfully!")
})


//Requiring application
const app = require('./app');

const port = process.env.PORT || 3000;

const server = app.listen(port, function(){
    console.log(`Listening to port ${port}...🙂` );
})

process.on("unhandledRejection", err => {
    if(process.env.NODE_ENV="development"){
        console.log(err)
    }else{
        console.log("UNHANDLED PROMISE REJECTION: ")
        console.log(`${err.name}: ${err.message}`);
    }
    server.close(()=>{
        process.exit(1);
    });
})